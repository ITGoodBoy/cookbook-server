package com.celestialapps.cookbook.config;


import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@ComponentScan
public class LocaleConfig extends WebMvcConfigurerAdapter{

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource  source = new ResourceBundleMessageSource();
        source.setBasenames("ValidationMessages");
        source.setDefaultEncoding("UTF-8");
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }

}
