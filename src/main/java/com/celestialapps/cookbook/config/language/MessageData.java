package com.celestialapps.cookbook.config.language;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageData {

    private MessageSource messageSource;

    @Autowired
    public MessageData(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getErrorMessage(String messageName) {
        return messageSource.getMessage(messageName, null, LocaleContextHolder.getLocale());
    }
}
