package com.celestialapps.cookbook;

import com.celestialapps.cookbook.service.category.Category;
import com.celestialapps.cookbook.service.category.CategoryRepository;
import com.celestialapps.cookbook.service.comment.CommentRepository;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithmRepository;
import com.celestialapps.cookbook.service.email.EmailSender;
import com.celestialapps.cookbook.service.email.EmailSenderRepository;
import com.celestialapps.cookbook.service.email.EmailSenderService;
import com.celestialapps.cookbook.service.ingredient.IngredientShort;
import com.celestialapps.cookbook.service.ingredient.IngredientShortRepository;
import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;
import com.celestialapps.cookbook.service.ingredient.ingredient.IngredientRepository;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.celestialapps.cookbook.service.recipe.Recipe;
import com.celestialapps.cookbook.service.recipe.RecipeRepository;
import com.celestialapps.cookbook.service.user.User;
import com.celestialapps.cookbook.service.user.UserRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.persistence.EntityManager;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootApplication
@Configuration
@EnableCaching
@EnableScheduling
public class CookbookApplication implements CommandLineRunner{

	@Autowired
	IngredientRepository ingredientRepository;
	@Autowired
	RecipeRepository recipeRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    EntityManager entityManager;
    @Autowired
    IngredientShortRepository ingredientShortRepository;
    @Autowired
    CookingAlgorithmRepository cookingAlgorithmRepository;
    @Autowired
    EmailSenderService emailSenderService;
    @Autowired
    EmailSenderRepository emailSenderRepository;


    @Override
    public void run(String... args) throws Exception {

    }

    private void update() {
        List<Category> categories = (List<Category>) categoryRepository.findAll();
        List<IngredientShort> ingredientShorts = (List<IngredientShort>) ingredientShortRepository.findAll();
        List<Recipe> recipes = (List<Recipe>) recipeRepository.findAll();


        categories.forEach(category -> category.setImgUrl("готовка/" + category.getName() + "/" + category.getName() + ".jpg"));
        System.out.println("completed one");
        ingredientShorts.forEach(ingredientShort -> ingredientShort.setImgUrl("ингредиенты/" + ingredientShort.getName() + "/" + ingredientShort.getName() + ".jpg"));
        System.out.println("completed two");
        recipes.forEach(recipe -> recipe.setImgUrl("готовка/" + recipe.getCategory().getName() + "/" + recipe.getName() + "/" + recipe.getName() + ".jpg"));
        System.out.println("completed three");

        categoryRepository.saveAll(categories);
        ingredientShortRepository.saveAll(ingredientShorts);
        recipeRepository.saveAll(recipes);

        System.out.println("completed all");
    }

    private void createRecipes() throws IOException {
        List<Category> categories = (List<Category>) categoryRepository.findAll();
        List<IngredientShort> ingredientShorts = (List<IngredientShort>) ingredientShortRepository.findAll();

        User user = userRepository.findById(1L).get();

        Map<String, Category> map = new HashMap<>();
        Map<String, IngredientShort> shortMap = new HashMap<>();

        categories.forEach(category -> map.put(category.getName(), category));
        ingredientShorts.forEach(ingredientShort -> shortMap.put(ingredientShort.getName(), ingredientShort));

        getAllRecipePaths().forEach(s -> {
            String name = s.substring(s.lastIndexOf("\\") + 1);

            String categoryName = s.substring(s.indexOf("готовка\\"), s.lastIndexOf("\\")).replaceFirst("готовка\\\\", "");

            String imgUrl = "Категории/" + categoryName + "/" + name + "/" + name + ".jpg";
            Category category = map.get(categoryName);

            List<Ingredient> ingredients = getIngredients(shortMap, s + "/" + "Ингредиенты.txt");
            List<CookingAlgorithm> cookingAlgorithms = getCookingAlgorithms(s + "/" + "Приготовление.txt");

            ingredients = (List<Ingredient>) ingredientRepository.saveAll(ingredients);

            cookingAlgorithms = (List<CookingAlgorithm>) cookingAlgorithmRepository.saveAll(cookingAlgorithms);

            Recipe recipe = new Recipe(name, "", cookingAlgorithms, imgUrl, category, user, ingredients);

            recipeRepository.save(recipe);
        });
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CookbookApplication.class, args);
    }




    private static List<Ingredient> getIngredients(Map<String, IngredientShort> ingredientShortMap, String ingredientsFile) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(ingredientsFile));

            return bufferedReader
                    .lines()
                    .map(s -> {
                        String name = s.substring(0, s.indexOf(" -"));
                        IngredientShort ingredientShort = ingredientShortMap.get(name);
                        String count = s.substring(s.indexOf("- ") + 2);

                        return new Ingredient(ingredientShort, count);
                    })
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<CookingAlgorithm> getCookingAlgorithms(String cookingAlgorithmsFile) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(cookingAlgorithmsFile));

            return bufferedReader.lines().map(s -> new CookingAlgorithm(s, "")).collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<String> getAllRecipePaths() throws IOException {

        List<Path> files = Files.list(new File("C:/Users/Sergey/Downloads/готовка").toPath()).collect(Collectors.toList());

        List<Path> allRecipePaths = new ArrayList<>();

        files.forEach(path -> {
            try {
                allRecipePaths.addAll(Files.list(path).collect(Collectors.toList()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return allRecipePaths
                .stream()
                .map(Path::toString).collect(Collectors.toList());
    }


}
