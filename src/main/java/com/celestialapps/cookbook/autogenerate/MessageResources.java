package com.celestialapps.cookbook.autogenerate;

import java.lang.String;

public class MessageResources {
  public static final String PASSWORD_SHORT = "password.short";

  public static final String INGREDIENT_NOT_FOUND = "ingredient.not.found";

  public static final String RECIPE_NOT_FOUND = "recipe.not.found";

  public static final String RECIPES_BY_NAME_NOT_FOUND = "recipes.by.name.not.found";
}
