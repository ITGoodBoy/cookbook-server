package com.celestialapps.cookbook.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceException extends Exception{

    ExceptionStatus exceptionStatus;

    public ServiceException(String message, ExceptionStatus exceptionStatus) {
        super(message);
        this.exceptionStatus = exceptionStatus;
    }

    public ServiceException(String message) {
        super(message);
    }


}
