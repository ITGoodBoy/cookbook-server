package com.celestialapps.cookbook.response;

import com.celestialapps.cookbook.service.ingredient.IngredientShort;
import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IngredientResponse {

    @JsonIgnore
    private long recipeId;
    private long id;
    private String name;
    private String count;
    private String imgUrl;

    public IngredientResponse(long recipeId, long id, String name, String imgUrl) {
        this.recipeId = recipeId;
        this.id = id;
        this.name = name;
        this.imgUrl = imgUrl;
    }

    //for create
    public IngredientResponse(Ingredient ingredient) {
        this.id = ingredient.getId();
        this.name = ingredient.getIngredientShort().getName();
        this.count = ingredient.getCount();
        this.imgUrl = ingredient.getIngredientShort().getImgUrl();
    }
}
