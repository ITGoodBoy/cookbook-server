package com.celestialapps.cookbook.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IngredientShortResponse {

    private long id;
    private String name;
    private String imgUrl;
}
