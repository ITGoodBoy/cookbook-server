package com.celestialapps.cookbook.controller;

import com.celestialapps.cookbook.autogenerate.MessageResources;
import com.celestialapps.cookbook.config.language.MessageData;
import com.celestialapps.cookbook.constants.HeaderConstants;
import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.request.SignInRequest;
import com.celestialapps.cookbook.request.SignUpConfirmRequest;
import com.celestialapps.cookbook.request.SignUpRequest;
import com.celestialapps.cookbook.service.user.User;
import com.celestialapps.cookbook.service.user.UserService;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/auth", headers = HttpHeaders.ACCEPT_LANGUAGE)
@Api(value = "Auth API Module", description = "Sign in, Sign up, Logout, Restore Password")
public class AuthController {

    private MessageData messageData;
    private UserService userService;

    @Autowired
    public AuthController(MessageData messageData, UserService userService) {
        this.messageData = messageData;
        this.userService = userService;
    }

    @PutMapping(value = "/login")
    public ResponseEntity<Token> login(@RequestBody SignInRequest signInRequest) throws ServiceException {
        return ResponseEntity.ok(userService.login(signInRequest));
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<Token> registration(@RequestBody SignUpRequest signUpRequest) throws ServiceException {
        return ResponseEntity.ok(userService.registration(signUpRequest));
    }

    @PutMapping(value = "/registration-confirm")
    public ResponseEntity<Token> registrationConfirm(@RequestBody SignUpConfirmRequest signUpConfirmRequest) throws ServiceException {
        return ResponseEntity.ok(userService.registrationConfirm(signUpConfirmRequest));
    }

    @GetMapping(value = "/is-token-active", headers = HeaderConstants.HEADER_TOKEN)
    public ResponseEntity<Boolean> isTokenActive() {
        User user;
        try {
            user = userService.getLoggedUser();
        } catch (Exception e) {
            user = null;
        }
        return ResponseEntity.ok(user != null);
    }

    @GetMapping(value = "/auth-token")
    public ResponseEntity<String> getAuthToken() {
        return ResponseEntity.ok(messageData.getErrorMessage(MessageResources.PASSWORD_SHORT));
    }





}