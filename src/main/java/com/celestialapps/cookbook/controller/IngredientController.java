package com.celestialapps.cookbook.controller;

import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;
import com.celestialapps.cookbook.service.ingredient.ingredient.IngredientService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/ingredients", headers = HttpHeaders.ACCEPT_LANGUAGE)
@Api(value = "Ingredient API Module", description = "getIngredient, getIngredients")
public class IngredientController {

    private IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Ingredient getIngredient(@PathVariable long id) throws ServiceException{
        return ingredientService.getIngredient(id);
    }

    @GetMapping("/")
    @ResponseBody
    public List<Ingredient> getIngredients() throws ServiceException {
        return ingredientService.getIngredients();
    }


}
