package com.celestialapps.cookbook.service.cooking_algorithm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class CookingAlgorithm {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    @Column(length = 1000)
    private String text;
    private String imgUrl;

    public CookingAlgorithm(String text, String imgUrl) {
        this.text = text;
        this.imgUrl = imgUrl;
    }
}
