package com.celestialapps.cookbook.service.email;

import com.celestialapps.cookbook.service.user.User;

public interface EmailSenderService {

    void sendSimpleHTML(String emailTo, String name, String messageText, String link);
    void sendSimpleMessage(String emailTo, String subject, String message);

    void sendRegistrationSuccess(User user, String code);
    void sendRestorePasswordCode(User user, String code);


}
