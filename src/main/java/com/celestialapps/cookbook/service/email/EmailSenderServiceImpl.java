package com.celestialapps.cookbook.service.email;

import com.celestialapps.cookbook.service.user.User;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

/**
 * Created by java-1-03 on 22.11.2017.
 */
@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    //@Value("${app.base.url}")
    private String BASE_URL = "THIS BASe URL TESt";

    private EmailSenderRepository emailSenderRepository;

    @Autowired
    public EmailSenderServiceImpl(EmailSenderRepository emailSenderRepository) {
        this.emailSenderRepository = emailSenderRepository;
    }

    @Override
    public void sendSimpleHTML(String emailTo, String name, String messageText, String partLink) {
        System.out.println("-------- USE_EMAIL_SENDER - ");
            String letter = readHTML();
            letter = letter.replaceAll("@username@", name);
            letter = letter.replaceAll("@message@", messageText);
            letter = letter.replaceAll("@link@", BASE_URL);

            MimeMessage message = new JavaMailSenderImpl().createMimeMessage();

            try {
                MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, true);
                mimeMessageHelper.setTo(emailTo);
                mimeMessageHelper.setSubject("Verification");
                mimeMessageHelper.setText(letter, true);
                sendMessage(message);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

    }


    @Override
    public void sendSimpleMessage(String emailTo, String subject, String message) {
        System.out.println("-------- USE_EMAIL_SENDER - ");
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(emailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        sendMessage(mailMessage);
    }

    @Override
    public void sendRegistrationSuccess(User user, String code) {
        sendSimpleHTML(user.getEmail(), user.getLogin(), "Thank you for registering! Use this - " + code + " for confirm registration. \nIf you did not do this click on this link: ", "");
    }

    @Override
    public void sendRestorePasswordCode(User user, String code) {
        sendSimpleMessage(user.getEmail(), "Reset password", "Use this - " + code + " code for change password.");
    }

    private void sendMessage(SimpleMailMessage simpleMailMessage) {
        prepareToSend().send(simpleMailMessage);

    }

    private void sendMessage(MimeMessage mimeMessage) {
        prepareToSend().send(mimeMessage);
    }


    private JavaMailSenderImpl prepareToSend() {
        EmailSender sender = emailSenderRepository
                .findAll(PageRequest.of(0, 1, new Sort(Sort.Direction.ASC, "sendCount")))
                .getContent()
                .get(0);

        sender.increaseSendCount();
        emailSenderRepository.save(sender);

        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setUsername(sender.getUserName());
        javaMailSender.setPassword(sender.getPassword());
        javaMailSender.setHost(sender.getHost());
        javaMailSender.setPort(sender.getPort());
        javaMailSender.setProtocol(sender.getProtocol());

        Properties properties = new Properties();
        properties.setProperty("spring.mail.properties.mail.smtp.auth", "true");
        properties.setProperty("spring.mail.properties.mail.smtp.starttls.enable", "true");

        javaMailSender.setJavaMailProperties(properties);

        return javaMailSender;
    }

    private String readHTML(){
        StringBuilder stringBuilder = new StringBuilder();
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext();

        Resource resource =
                appContext.getResource("classpath:" + "template/email.html");

        try{
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            br.close();

        }catch(IOException e){
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
