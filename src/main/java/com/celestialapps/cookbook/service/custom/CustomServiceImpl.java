package com.celestialapps.cookbook.service.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomServiceImpl implements CustomService {

    private CustomRepository customRepository;

    @Autowired
    public CustomServiceImpl(CustomRepository customRepository) {
        this.customRepository = customRepository;
    }

    @Override
    public long getCurrentDatabaseSizeInMB() {
        return customRepository.findById(1L).get().getAllSizeInMB();
    }
}
