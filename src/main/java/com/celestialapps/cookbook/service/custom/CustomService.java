package com.celestialapps.cookbook.service.custom;

public interface CustomService {

    long getCurrentDatabaseSizeInMB();
}
