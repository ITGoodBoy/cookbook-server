package com.celestialapps.cookbook.service.custom;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class CustomSchedule {

    private CustomRepository customRepository;

    @Autowired
    public CustomSchedule(CustomRepository customRepository) {
        this.customRepository = customRepository;
    }

    @Scheduled(cron = "0 0/30 * * * ?")
    public void resetAllSize() {
        System.out.println("resetAllSize start");
        long storageSize = getStorageSizeInMB();
        if (storageSize != 0) {
            Custom custom = new Custom(1, customRepository.getDatabaseSumInMB() + storageSize);
            customRepository.save(custom);
        }
        System.out.println("resetAllSize end");
    }


    private long getStorageSizeInMB() {
        Storage storage;
        try {
            storage = StorageOptions.newBuilder().setProjectId("firebaseId")
                    .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("C:\\Users\\Sergey\\IdeaProjects\\cookbook\\src\\main\\resources\\CookBook-6f92b99a72fb.json")))
                    .build()
                    .getService();
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

        Bucket bucket = storage.get("cookbook-a8d43.appspot.com");

        AtomicLong atomicLong = new AtomicLong();

        Page<Blob> blobPage = bucket.list(Storage.BlobListOption.fields(Storage.BlobField.SIZE));
        blobPage.iterateAll().forEach(blob -> atomicLong.addAndGet(blob.getSize()));

        return humanReadableByteCount(atomicLong.get());
    }

    private long humanReadableByteCount(long bytes) {
        return (bytes /(1000 * 1000));
    }
}
