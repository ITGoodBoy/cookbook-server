package com.celestialapps.cookbook.service.recipe;

import com.celestialapps.cookbook.request.CookingAlgorithmRequest;
import com.celestialapps.cookbook.request.IngredientRequest;
import com.celestialapps.cookbook.request.RecipeRequest;
import com.celestialapps.cookbook.service.category.Category;
import com.celestialapps.cookbook.service.comment.Comment;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;

import com.celestialapps.cookbook.service.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
public class Recipe implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String name;
    private Date date = new Date(System.currentTimeMillis());
    private String videoId;
    private long likeCount;
    @OneToMany(orphanRemoval = true)
    private List<CookingAlgorithm> cookingAlgorithms;
    private boolean isVegetarian;
    private boolean isVegan;
    private boolean isPublished;
    @Column(unique = true)
    private String imgUrl;
    @ManyToOne
    @JoinColumn(name="category_id")
    @JsonBackReference
    private Category category;
    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonManagedReference
    private User user;
    @OneToMany(orphanRemoval = true)
    @JsonManagedReference
    private List<Ingredient> ingredients;
    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Comment> comments;

    public  Recipe(long id) {
        this.id = id;
    }


    public Recipe(String name, String videoId, List<CookingAlgorithm> cookingAlgorithms, String imgUrl, Category category, User user, List<Ingredient> ingredients) {
        this.name = name;
        this.videoId = videoId;
        this.cookingAlgorithms = cookingAlgorithms;
        this.imgUrl = imgUrl;
        this.category = category;
        this.user = user;
        this.ingredients = ingredients;

        this.comments = new ArrayList<>();
        this.isVegan = isVegan(ingredients);
        this.isVegetarian = isVegetarian(ingredients);
    }

    private boolean isVegan(List<Ingredient> ingredients) {
        for (Ingredient ingredient : ingredients) {
            if (!ingredient.isVegan()) return false;
        }

        return true;
    }

    private boolean isVegetarian(List<Ingredient> ingredients) {
        for (Ingredient ingredient : ingredients) {
            if (!ingredient.isVegetarian()) return false;
        }

        return true;
    }
}
