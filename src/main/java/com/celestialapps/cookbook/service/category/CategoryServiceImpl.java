package com.celestialapps.cookbook.service.category;

import com.celestialapps.cookbook.response.CategoryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    @Override
    public List<CategoryResponse> getAllCategoryResponses() {
        return categoryRepository.findAllCategoryShorts();
    }
}
