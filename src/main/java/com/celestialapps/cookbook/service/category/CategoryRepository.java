package com.celestialapps.cookbook.service.category;

import com.celestialapps.cookbook.response.CategoryResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {

    Category findByName(String name);

    @Query("select new com.celestialapps.cookbook.response.CategoryResponse(c.id, c.name, c.imgUrl) from Category c")
    List<CategoryResponse> findAllCategoryShorts();


    boolean existsByName(String name);
}
