package com.celestialapps.cookbook.service.profile;

import com.celestialapps.cookbook.exception.ServiceException;

public interface ProfileService {

    void addProfile(Profile profile) throws ServiceException;
    Profile getProfile(long id) throws ServiceException;

}
