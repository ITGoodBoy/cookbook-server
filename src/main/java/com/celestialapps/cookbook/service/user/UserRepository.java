package com.celestialapps.cookbook.service.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmail(String email);
    User findByLoginOrEmail(String login, String email);

    boolean existsByEmail(String email);
    boolean existsByEmailAndEnabledTrue(String email);
    boolean existsByLoginAndEnabledTrue(String login);
    boolean existsByLogin(String login);
    boolean existsByLoginOrEmail(String login, String email);

}
