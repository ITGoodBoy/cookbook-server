package com.celestialapps.cookbook.service.user.auth.token;

import com.celestialapps.cookbook.service.user.User;

public interface TokenService {


    Token generateToken(User user);
    User getUserByTokenString(String tokenString);
    boolean deleteByTokenString(String tokenStrin);
    Token getTokenByTokenString(String tokenString);
}
