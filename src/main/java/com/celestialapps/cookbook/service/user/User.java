package com.celestialapps.cookbook.service.user;

import com.celestialapps.cookbook.service.comment.Comment;
import com.celestialapps.cookbook.service.profile.Profile;
import com.celestialapps.cookbook.service.recipe.Recipe;
import com.celestialapps.cookbook.service.user.auth.Role;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.temp.TempData;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(of = {"login", "email"})
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    @Column(unique = true)
    private String login;
    @Column(unique = true)
    private String email;
    @Column(unique = false)
    private String imgUrl;
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date createAt = new Date(System.currentTimeMillis());
    @JsonIgnore
    private String password;
    @OneToOne(mappedBy = "user")
    private Token token;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "auth_role", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Set<Role> roles = new HashSet<>();
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Comment> comments;
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    @JsonBackReference
    private List<Recipe> recipes;
    @OneToMany
    private List<Recipe> favouriteRecipes;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = false;

    public User(String login, String email, String password, Role... role) {
        this.login = login;
        this.email = email;
        this.password = password;
        roles.addAll(Arrays.asList(role));
    }

    public User(String login, String email, String imgUrl, String password, List<Comment> comments, List<Recipe> recipes, List<Recipe> favouriteRecipes, Role... role) {
        this.login = login;
        this.email = email;
        this.imgUrl = imgUrl;
        this.password = password;
        this.comments = comments;
        this.recipes = recipes;
        this.favouriteRecipes = favouriteRecipes;
        roles.addAll(Arrays.asList(role));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Role role : roles) {
            String name = role.name().toUpperCase();
            if(!name.startsWith("ROLE_"))
                name = "ROLE_"+name;
            grantedAuthorities.add(new SimpleGrantedAuthority(name));
        }
        return grantedAuthorities;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
//                ", token=" + token +
                ", authorities=" + Arrays.toString(roles.stream().map(Enum::name).toArray()) +
                ", accountNonExpired=" + accountNonExpired +
                ", accountNonLocked=" + accountNonLocked +
                ", credentialsNonExpired=" + credentialsNonExpired +
                ", enabled=" + enabled +
                '}';
    }
}
