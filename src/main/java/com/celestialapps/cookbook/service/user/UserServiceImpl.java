package com.celestialapps.cookbook.service.user;

import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.request.SignInRequest;
import com.celestialapps.cookbook.request.SignUpConfirmRequest;
import com.celestialapps.cookbook.request.SignUpRequest;
import com.celestialapps.cookbook.service.email.EmailSenderService;
import com.celestialapps.cookbook.service.user.auth.Credentials;
import com.celestialapps.cookbook.service.user.auth.Role;
import com.celestialapps.cookbook.service.user.auth.provider.Provider;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import com.celestialapps.cookbook.service.user.temp.TempData;
import com.celestialapps.cookbook.service.user.temp.TempDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Random;

@Service
public class UserServiceImpl implements UserService {

    private Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final TempDataRepository tempDataRepository;
    private final TokenService tokenService;
    private final PasswordEncoder passwordEncoder;
    private final EmailSenderService emailSenderService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, TempDataRepository tempDataRepository,
                           TokenService tokenService, PasswordEncoder passwordEncoder, EmailSenderService emailSenderService) {
        this.userRepository = userRepository;
        this.tempDataRepository = tempDataRepository;
        this.tokenService = tokenService;
        this.passwordEncoder = passwordEncoder;
        this.emailSenderService = emailSenderService;
    }

    @Override
    public User findById(long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public Token login(SignInRequest signInRequest) throws ServiceException {

        String email = signInRequest.getEmail();
        String password = signInRequest.getPassword();

        if (!userRepository.existsByEmail(email)) {
            throw new ServiceException("Пользователь с данным Email не зарегестрирован");
        }

        User user = userRepository.findByEmail(email);

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new ServiceException("Неправельный пароль");
        }

        return tokenService.generateToken(user);
    }

    @Override
    public Token registration(SignUpRequest signUpRequest) throws ServiceException{

        String login = signUpRequest.getLogin();
        String email = signUpRequest.getEmail();

        if (userRepository.existsByLogin(login)) {
            throw new ServiceException("Пользователь с данным логином уже существует");
        }

        if (userRepository.existsByEmail(email)) {
            throw new ServiceException("Пользователь с данным email адрессом уже существует");
        }

        User user = new User(login, email, "",
                passwordEncoder.encode(signUpRequest.getPassword()), new ArrayList<>(),
                new ArrayList<>(), new ArrayList<>(), Role.USER);

        userRepository.save(user);

        return tokenService.generateToken(user);
    }

    @Override
    public Token registrationConfirm(SignUpConfirmRequest signUpConfirmRequest) throws ServiceException{

        String email = signUpConfirmRequest.getEmail();
        String code = signUpConfirmRequest.getCode();

        if (!userRepository.existsByEmail(email)) {
            throw new ServiceException("Данный email не был зарегестрирован");
        }

        if (userRepository.existsByEmailAndEnabledTrue(email)) {
            throw new ServiceException("Данный email уже подтверждён");
        }

        if (!tempDataRepository.existsByEmail(email)) {
            throw new ServiceException("Код подтверждения не был отправлен на данный email");
        }

        TempData tempData = tempDataRepository.findByEmail(email);

        if (!tempData.getCode().equals(code)) {

       //     if (tempData.getNumberOfInputs() >= 12) {
         //       tempDataRepository.delete(tempData);
        //        throw new ServiceException("Код был введён неправильно более 12 раз, Отправте код подтверждения повторно");
        //    } else {
                tempData.incrementNumberOfInputs();
                tempDataRepository.save(tempData);
          //  }

            throw new ServiceException("codes not equals");
        }

        User user = userRepository.findByEmail(email);
        user.setEnabled(true);

        userRepository.save(user);
        tempDataRepository.delete(tempData);

        return tokenService.generateToken(user);
    }

    @Override
    public void restorePassword(String email)  {
//        try {
//            UserDetails userDetails = userRepository.findByEmail(email);
//            if (userDetails == null)
//                throw new ServiceException("User not found!", HttpStatus.NOT_FOUND);
//
//            String code = (new Random().nextInt(9000) + 1000) + "";
//
//            Element element = new Element(email, code);
//            log.info("ElementCode: " + element.toString());
//
//            cacheInterface.save(CacheStorage.Type.Code, element);
//            emailSenderService.sendRestorePasswordCode((User) userDetails, code);
//        } catch (Exception e) {
//            throw new ServiceException(e);
//        }
    }

    @Override
    public void changePassword(Credentials.ResetPassword credentials) {
//        try {
//            log.info(credentials.toString());
//            User user = userRepository.findByLoginOrEmail(credentials.getEmail(), credentials.getEmail());
//            if (user == null)
//                throw new ServiceException("User not found", HttpStatus.NOT_FOUND);
//
//            Element element = new Element(credentials.getEmail(), credentials.getCode());
//            if (!cacheInterface.deleteByElement(CacheStorage.Type.Code, element))
//                throw new ServiceException("Email or code incorrect!", HttpStatus.NOT_ACCEPTABLE);
//
//            user.setPassword(new BCryptPasswordEncoder().encode(credentials.getPassword()));
//            userRepository.save(user);
//        } catch (Exception e) {
//            throw new ServiceException(e);
//        }
    }

    @Override
    public void changePassword(Credentials.SetNewPassword credentials, User user) {
//
//
//
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//
//        if(bCryptPasswordEncoder.matches(credentials.getOldPassword(), user.getPassword())){
//            if(credentials.getNewPassword().equals(credentials.getRepeatPassword())){
//                user.setPassword(bCryptPasswordEncoder.encode(credentials.getNewPassword()));
//                userRepository.save(user);
//            }
//            else {
//                throw new ServiceException("New and repeat password did not match", HttpStatus.UNAUTHORIZED);
//            }
//        }else {
//            throw new ServiceException("Wrong password", HttpStatus.UNAUTHORIZED);
//        }
    }

    @Override
    public Token socialLogin(String token, String secret, Provider.Type type) {
        Provider provider = Provider.create(userRepository, tokenService, type);
        return provider.login(token, secret);
    }

    @Override
    public User getLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (User) authentication.getPrincipal();
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }



    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email);
    }
}
