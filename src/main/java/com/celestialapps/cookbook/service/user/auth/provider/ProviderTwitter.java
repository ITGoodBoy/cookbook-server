package com.celestialapps.cookbook.service.user.auth.provider;

import com.celestialapps.cookbook.service.user.UserRepository;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

class ProviderTwitter extends Provider {

    private static final String URL = "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true";
    private final String appIdTwitter = "7zvb58GRpb6aFjQTT9CamjAFr";
    private final String appSecretTwitter = "kZLDzedvns1dPp0SGrwEdJWQdrA110cUr8GuerruVlbcuJg0v2";

    ProviderTwitter(UserRepository userRepository, TokenService tokenService) {
        super(userRepository, tokenService);
    }

    @Override
    public Token login(String token, String secret)  {
        try {
            RestTemplate restTemplate = new TwitterTemplate(appIdTwitter, appSecretTwitter, token, secret).getRestTemplate();
            TwitterProfileWithEmail profileWithEmail = restTemplate.getForObject(URL, TwitterProfileWithEmail.class);

            String email = profileWithEmail.getEmail();
            Token permToken = registerOrLogin(email);

            return Optional.of(permToken).orElseThrow(() -> new UnsupportedOperationException("Twitter auth fail"));
        } catch (Exception e) {
            throw new UnsupportedOperationException("Twitter auth fail", e);
        }
    }
}
