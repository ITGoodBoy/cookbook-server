package com.celestialapps.cookbook.service.user.temp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class TempData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String email;
    private String code;
    private int numberOfInputs;

    public TempData(String email, String code) {
        this.email = email;
        this.code = code;
    }


    public void incrementNumberOfInputs() {
        ++numberOfInputs;
    }
}
