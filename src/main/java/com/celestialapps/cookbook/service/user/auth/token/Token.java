package com.celestialapps.cookbook.service.user.auth.token;

import com.celestialapps.cookbook.service.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    @JsonIgnore
    private long id;
    @JsonIgnore
    private Date createdAt;
    @OneToOne
    @JsonIgnore
    private User user;
    @Column(unique = true)
    private String tokenString;

    public Token(User user, String tokenString) {
        this.createdAt = new Date(System.currentTimeMillis());
        this.user = user;
        this.tokenString = tokenString;
    }

}
