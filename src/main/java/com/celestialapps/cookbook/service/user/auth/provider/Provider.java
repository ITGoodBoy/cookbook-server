package com.celestialapps.cookbook.service.user.auth.provider;

import com.celestialapps.cookbook.service.user.User;
import com.celestialapps.cookbook.service.user.UserRepository;
import com.celestialapps.cookbook.service.user.auth.Role;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class Provider {

    public enum Type {
        FACEBOOK,
        GOOGLE,
        TWITTER
    }

    private UserRepository userRepository;
    private TokenService tokenService;

    Provider(UserRepository userRepository, TokenService tokenService) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
    }

    public static Provider create(UserRepository userRepository, TokenService tokenService, Provider.Type type) {
        switch (type){
            case FACEBOOK: return new ProviderFacebook(userRepository, tokenService);
            case GOOGLE: return new ProviderGoogle(userRepository, tokenService);
            case TWITTER: return new ProviderTwitter(userRepository, tokenService);
            default: throw new UnsupportedOperationException("Provider not found");
        }
    }

    public abstract Token login(String token, String secret);

    Token registerOrLogin(String email) {
        if (email != null && !email.isEmpty()) {
            User user = Optional.ofNullable(userRepository.findByLoginOrEmail(email, email)).orElseGet(() -> {
                User u = new User(email, email, "", Role.USER);
                userRepository.save(u);
                return u;
            });
            return tokenService.generateToken(user);
        }
        return null;
    }
}
