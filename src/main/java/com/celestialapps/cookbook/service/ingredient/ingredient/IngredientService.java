package com.celestialapps.cookbook.service.ingredient.ingredient;

import com.celestialapps.cookbook.exception.ServiceException;

import java.util.List;

public interface IngredientService {


    Ingredient getIngredient(long id) throws ServiceException;
    Ingredient getIngredient(String name) throws ServiceException;
    List<Ingredient> getIngredients() throws ServiceException;

}
