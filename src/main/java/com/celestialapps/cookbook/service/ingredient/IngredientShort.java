package com.celestialapps.cookbook.service.ingredient;

import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;
import com.celestialapps.cookbook.service.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Sergey on 07.03.2018.
 */

@Entity
@Data
@NoArgsConstructor
public class IngredientShort implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    @Column(unique = true)
    private String name;
    @Column(unique = true)
    private String imgUrl;
    @OneToOne
    @JsonBackReference
    private User user;
    private boolean isVegetarian;
    private boolean isVegan;

    public IngredientShort(String name, String imgUrl, User user) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.user = user;
    }
}
