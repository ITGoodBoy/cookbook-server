package com.celestialapps.cookbook.service.ingredient.ingredient;

import com.celestialapps.cookbook.service.ingredient.IngredientShort;
import com.celestialapps.cookbook.service.recipe.Recipe;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Ingredient implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @JsonBackReference
    private Recipe recipe;
    @OneToOne(targetEntity = IngredientShort.class)
    private IngredientShort ingredientShort;
    private String count;
    private boolean isVegetarian;
    private boolean isVegan;


    public Ingredient(IngredientShort ingredientShort, String count) {
        this.ingredientShort = ingredientShort;
        this.count = count;

        this.isVegetarian = ingredientShort.isVegetarian();
        this.isVegan = ingredientShort.isVegan();
    }
}
