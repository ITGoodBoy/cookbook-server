package com.celestialapps.cookbook.service.ingredient;

import com.celestialapps.cookbook.response.IngredientShortResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IngredientShortService {

    List<IngredientShortResponse> getAllIngredientShortResponses();
}
